﻿
#ifndef GATESERIAL_H
#define GATESERIAL_H

#include <QWidget>
#include<QTimer>
#include <QByteArray>
#include <QSerialPortInfo>
#include<QDebug>
#include <QThread>
#include<QTime>
#include <QDateTime>
#include<QLabel>
#include "qextserialport.h"
#include <QObject>



//针对道闸进行单独控制的串口对象逻辑

class gateSerial : public QObject
{
    Q_OBJECT
public:
    explicit gateSerial(QObject *parent = nullptr);
    ~gateSerial();

private :
    bool g_bcomOk;//串口是否打开
    bool g_bGateConn;//是否与道闸串口连接

    QextSerialPort * g_gateCom;//道闸串口
    QTimer * g_timerRead;//定时读
    QTimer * g_timerWrite;//定时发送数据
    int g_sleepTime;                   //接收延时时间
    int g_fiBufferSize;  //当前缓存区长度
    QByteArray *g_byteBuffer; //接收缓冲区
    QList<QByteArray> g_listSendCmd;//指令队列
    QList<QByteArray>g_listTmpCmd;//用于接收一些临时指令缓冲(这个一般用于比较复杂的指令，如二次确认等)
    QList<QByteArray>g_listControlCmd;    //固定控制指令


private slots:
  //  void gateOnSerialOpen();        //串口打开、关闭
    void gateOnTimerReadCmd();//定时读取串口数据
    void gateOnTimerSendCmd();//定时发送指令
    void gateOnReplyCmdParse();//回应报文缓冲区解析

public slots:
    //ui -slots
    void openGateSerial(const QString & name);
    void closeGateSerial();
    void raiseGate();//开
    void lowerGate();//关
    void scramGate();//急停
    void alwaysOpenGate();//常开
    void cancleAlwOpenGate();//取消常开
    void alwaysCloseGate();//对开只开主闸
    //thread
    void initGateSerial();//初始化 --线程初始化start信号绑定

private:

    quint16  CRC16_check(const QByteArray & data,int len);//crc16校验
    void sendCmdToList(int num);//指令发送
signals:
//    void  signal_gateStatus(int num);
//    void  signal_uiControl(UI_control cmd);
//    void logDAta(int type ,QString data);



};

#endif // GATESERIAL_H
