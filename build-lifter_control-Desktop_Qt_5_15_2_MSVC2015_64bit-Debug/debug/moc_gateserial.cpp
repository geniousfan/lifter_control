/****************************************************************************
** Meta object code from reading C++ file 'gateserial.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../lifter_control/gateserial.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gateserial.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_gateSerial_t {
    QByteArrayData data[15];
    char stringdata0[200];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_gateSerial_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_gateSerial_t qt_meta_stringdata_gateSerial = {
    {
QT_MOC_LITERAL(0, 0, 10), // "gateSerial"
QT_MOC_LITERAL(1, 11, 18), // "gateOnTimerReadCmd"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 18), // "gateOnTimerSendCmd"
QT_MOC_LITERAL(4, 50, 19), // "gateOnReplyCmdParse"
QT_MOC_LITERAL(5, 70, 14), // "openGateSerial"
QT_MOC_LITERAL(6, 85, 4), // "name"
QT_MOC_LITERAL(7, 90, 15), // "closeGateSerial"
QT_MOC_LITERAL(8, 106, 9), // "raiseGate"
QT_MOC_LITERAL(9, 116, 9), // "lowerGate"
QT_MOC_LITERAL(10, 126, 9), // "scramGate"
QT_MOC_LITERAL(11, 136, 14), // "alwaysOpenGate"
QT_MOC_LITERAL(12, 151, 17), // "cancleAlwOpenGate"
QT_MOC_LITERAL(13, 169, 15), // "alwaysCloseGate"
QT_MOC_LITERAL(14, 185, 14) // "initGateSerial"

    },
    "gateSerial\0gateOnTimerReadCmd\0\0"
    "gateOnTimerSendCmd\0gateOnReplyCmdParse\0"
    "openGateSerial\0name\0closeGateSerial\0"
    "raiseGate\0lowerGate\0scramGate\0"
    "alwaysOpenGate\0cancleAlwOpenGate\0"
    "alwaysCloseGate\0initGateSerial"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_gateSerial[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x08 /* Private */,
       3,    0,   75,    2, 0x08 /* Private */,
       4,    0,   76,    2, 0x08 /* Private */,
       5,    1,   77,    2, 0x0a /* Public */,
       7,    0,   80,    2, 0x0a /* Public */,
       8,    0,   81,    2, 0x0a /* Public */,
       9,    0,   82,    2, 0x0a /* Public */,
      10,    0,   83,    2, 0x0a /* Public */,
      11,    0,   84,    2, 0x0a /* Public */,
      12,    0,   85,    2, 0x0a /* Public */,
      13,    0,   86,    2, 0x0a /* Public */,
      14,    0,   87,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void gateSerial::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<gateSerial *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->gateOnTimerReadCmd(); break;
        case 1: _t->gateOnTimerSendCmd(); break;
        case 2: _t->gateOnReplyCmdParse(); break;
        case 3: _t->openGateSerial((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->closeGateSerial(); break;
        case 5: _t->raiseGate(); break;
        case 6: _t->lowerGate(); break;
        case 7: _t->scramGate(); break;
        case 8: _t->alwaysOpenGate(); break;
        case 9: _t->cancleAlwOpenGate(); break;
        case 10: _t->alwaysCloseGate(); break;
        case 11: _t->initGateSerial(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject gateSerial::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_gateSerial.data,
    qt_meta_data_gateSerial,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *gateSerial::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *gateSerial::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_gateSerial.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int gateSerial::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
